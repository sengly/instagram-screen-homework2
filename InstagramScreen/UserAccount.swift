//
//  UserAccount.swift
//  InstagramScreen
//
//  Created by Sengly Sun on 11/24/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//

import Foundation

struct UserAccount {
    var profilePicture: String
    var userName: String
    var contenPicture: String
    var likeNumber: String
    var caption: String
}


let allUserAccount = [
    UserAccount(profilePicture: "lisa_pf", userName: "lalalalisa_m", contenPicture: "lisa", likeNumber: "101 likes", caption: "Black Day!!"),
    UserAccount(profilePicture: "baifern_pf", userName: "baifernbah", contenPicture: "baifern_ct", likeNumber: "120 likes", caption: "Be yourself..."),
    UserAccount(profilePicture: "meansonyta_pf", userName: "meansonyta", contenPicture: "meansonyta_ct", likeNumber: "404 likes", caption: "នារីខ្មែរ!!"),
    UserAccount(profilePicture: "chippy_pf", userName: "chippy_chippy", contenPicture: "chippy_ct", likeNumber: "405 likes", caption: "Hello Hi Bye @@")
]

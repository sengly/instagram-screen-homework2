//
//  IGTableViewCell.swift
//  InstagramScreen
//
//  Created by Sengly Sun on 11/23/19.
//  Copyright © 2019 Sengly Sun. All rights reserved.
//

import UIKit

extension UIImageView {

   func makeRounded() {
    let radius = self.frame.width / 2
    self.layer.cornerRadius = radius
    self.layer.masksToBounds = true
    self.clipsToBounds = true
    self.contentMode = .scaleAspectFill
   }
    
}

class IGTableViewCell: UITableViewCell {

    @IBAction func optionPressed(_ sender: Any) {
    }
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var contentImage: UIImageView!
    @IBOutlet weak var profileImage: UIImageView!
    
    @IBAction func likePressed(_ sender: Any) {
    }
    @IBAction func commentPressed(_ sender: Any) {
    }
    @IBAction func sharePressed(_ sender: Any) {
    }
    @IBAction func savePressed(_ sender: Any) {
    }
    @IBOutlet weak var likedLabel: UILabel!
    @IBOutlet weak var captionLabel: UILabel!
        override func awakeFromNib() {
        super.awakeFromNib()
        profileImage.makeRounded()
        contentImage.contentMode = .scaleAspectFill
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    
    
}
